package com.example.rakhi.ngoconnect;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.series.BarGraphSeries;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;

import java.util.Random;

public class ShowGraphStandard extends AppCompatActivity {
    GraphView graph;
    String text = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.graph_standard);

        graph = (GraphView) findViewById(R.id.graph);

        Random r = new Random();

        DataPoint[] d = new DataPoint[5];
        int ar[]={2010,2011,2012,2013,2014};
        for (int k = 0; k < 5; k++) {
            d[k] = new DataPoint(ar[k], r.nextInt(30));
        }

        TextView textView = (TextView) findViewById(R.id.txt);
        textView.setText("X: Years || Y: Percentage");

        LineGraphSeries<DataPoint> series1 = new LineGraphSeries<DataPoint>(d);

        series1.setColor(R.color.colorAccent);
        graph.addSeries(series1);


    }
}