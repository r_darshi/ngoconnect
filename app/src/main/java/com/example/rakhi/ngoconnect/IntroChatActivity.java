package com.example.rakhi.ngoconnect;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.DataSetObserver;
import android.media.Image;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AbsListView;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class IntroChatActivity extends AppCompatActivity {

    ImageButton sendMessageButton;
    EditText editMessage;
    ChatBotAdapter adapter;
    ListView chatList;
    Questions qq;

    ArrayList<ChatMessage> messageArrayList;
    ArrayList<String> ans;

    int post = 0;

    boolean side = true, chk_qnFinish = false;
    private Handler mHandler = new Handler();
    // String name = "Nishijeet";
    int k = 0;
    String typed_msg;
    int branch = -1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_intro_chat);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);


        qq = new Questions();

        ans = new ArrayList<>();

        messageArrayList = new ArrayList<ChatMessage>();

        sendMessageButton = (ImageButton) findViewById(R.id.sendMessageButton);
        editMessage = (EditText) findViewById(R.id.messageEditText);
        chatList = (ListView) findViewById(R.id.chat_list);
        adapter = new ChatBotAdapter(this, R.layout.message_row, messageArrayList);
        chatList.setAdapter(adapter);

        messageArrayList.add(qq.qintro);
        adapter.notifyDataSetChanged();
        post++;
        messageArrayList.add(qq.typ);
        adapter.notifyDataSetChanged();
        mHandler.postDelayed(new Runnable() {
            public void run() {
                messageArrayList.set(post, qq.q0);
                adapter.notifyDataSetChanged();
                post++;
                k++;                       ///////////////////////////////////////////k++
                chk_qnFinish = true;

            }
        }, 2000);


        editMessage.setText("");


        editMessage.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if ((event.getAction() == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER)) {
                    return sendChatMessage();
                }
                return false;
            }
        });
        sendMessageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                sendChatMessage();
                if (chk_qnFinish == true) {
                    chk_qn(k);

                }
            }
        });


        chatList.setTranscriptMode(AbsListView.TRANSCRIPT_MODE_ALWAYS_SCROLL);

        //to scroll the list view to bottom on data change
        adapter.registerDataSetObserver(new DataSetObserver() {
            @Override
            public void onChanged() {
                super.onChanged();
                chatList.setSelection(adapter.getCount() - 1);
            }
        });


    }


    public void tst(String qn, String ans) {
        Log.i("QUEANS", qn + "::" + ans);

    }


    private boolean sendChatMessage() {


        Log.i("sendMessageChat", editMessage.getText().toString());
        typed_msg = editMessage.getText().toString();
        if (!editMessage.getText().toString().isEmpty()) {
            messageArrayList.add(new ChatMessage(side, editMessage.getText().toString()));
            adapter.notifyDataSetChanged();
            post++;
            editMessage.setText("");
            //side = !side;
            Log.i("sendMessageChatCondn", editMessage.getText().toString());
        }
        return true;
    }

    final boolean[] chkt = {false};
    final boolean[] chkt1 = {false};

    public void chk_qn(final int kk) {
        final String s = typed_msg.toLowerCase().trim();


        if (kk == 1) {

            if (!s.isEmpty()) {
                chk_qnFinish = false;

                ans.add(s);
                messageArrayList.add(qq.typ);
                adapter.notifyDataSetChanged();
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        messageArrayList.set(post, qq.okay);
                        adapter.notifyDataSetChanged();
                        post++;
                        messageArrayList.add(qq.typ);
                        adapter.notifyDataSetChanged();
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                k++;
                                messageArrayList.set(post, qq.q1);

                                branch = 21;
                                adapter.notifyDataSetChanged();
                                post++;

                                chk_qnFinish = true;

                            }
                        }, 1000);
                    }
                }, 1000);

            } else {
                messageArrayList.add(qq.typ);
                adapter.notifyDataSetChanged();
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        messageArrayList.set(post, qq.error);
                        adapter.notifyDataSetChanged();
                        post++;
                    }
                }, 700);
            }
        }

        if (kk == 2) {

            if (branch == 11) {
            } else if (branch == 21) {


                if (!s.isEmpty()) {

                    ans.add(s);
                    chk_qnFinish = false;

                    messageArrayList.add(qq.typ);
                    adapter.notifyDataSetChanged();
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            messageArrayList.set(post, qq.okay6);
                            adapter.notifyDataSetChanged();
                            post++;
                            messageArrayList.add(qq.typ);
                            adapter.notifyDataSetChanged();
                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    branch = 21;
                                    k++;
                                    messageArrayList.set(post, qq.q3);
                                    adapter.notifyDataSetChanged();
                                    post++;
                                    chk_qnFinish = true;

                                }
                            }, 1000);
                        }
                    }, 1000);

                } else {
                    messageArrayList.add(qq.typ);
                    adapter.notifyDataSetChanged();
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            messageArrayList.set(post, qq.error);
                            adapter.notifyDataSetChanged();
                            post++;
                        }
                    }, 700);
                }
            }


        }

        if (kk == 3) {

            if (branch == 11) {
            } else if (branch == 21) {


                if (!s.isEmpty()) {
                    ans.add(s);
                    chk_qnFinish = false;

                    messageArrayList.add(qq.typ);
                    adapter.notifyDataSetChanged();
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            messageArrayList.set(post, qq.okay6);
                            adapter.notifyDataSetChanged();
                            post++;
                            messageArrayList.add(qq.typ);
                            adapter.notifyDataSetChanged();
                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {

                                    k++;
                                    messageArrayList.set(post, qq.q4);
                                    adapter.notifyDataSetChanged();
                                    post++;
                                    chk_qnFinish = true;

                                }
                            }, 1000);
                        }
                    }, 1000);

                } else {
                    messageArrayList.add(qq.typ);
                    adapter.notifyDataSetChanged();
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            messageArrayList.set(post, qq.error);
                            adapter.notifyDataSetChanged();
                            post++;
                        }
                    }, 700);
                }
            }


        }

        if (kk == 4) {

            if (branch == 11) {
            } else if (branch == 21) {


                if (!s.isEmpty()) {
                    ans.add(s);
                    chk_qnFinish = false;

                    messageArrayList.add(qq.typ);
                    adapter.notifyDataSetChanged();
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            messageArrayList.set(post, qq.okay);
                            adapter.notifyDataSetChanged();
                            post++;
                            messageArrayList.add(qq.typ);
                            adapter.notifyDataSetChanged();
                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {

                                    k++;
                                    messageArrayList.set(post, qq.q5);
                                    adapter.notifyDataSetChanged();
                                    post++;
                                    chk_qnFinish = true;

                                }
                            }, 1000);
                        }
                    }, 1000);

                } else {
                    messageArrayList.add(qq.typ);
                    adapter.notifyDataSetChanged();
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            messageArrayList.set(post, qq.error);
                            adapter.notifyDataSetChanged();
                            post++;
                        }
                    }, 700);
                }
            }


        }


        if (kk == 5) {
            if (branch == 5) {
            } else if (branch == 21) {

                if (!s.isEmpty()) {
                    chk_qnFinish = false;
                    ans.add(s);
                    messageArrayList.add(qq.typ);
                    adapter.notifyDataSetChanged();
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            messageArrayList.set(post, qq.okay3);
                            adapter.notifyDataSetChanged();
                            post++;
                            messageArrayList.add(qq.typ);
                            adapter.notifyDataSetChanged();
                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    k++;

                                    messageArrayList.set(post, qq.q6);
                                    adapter.notifyDataSetChanged();
                                    post++;
                                    chk_qnFinish = true;

                                }
                            }, 1000);
                        }
                    }, 1000);

                } else {
                    messageArrayList.add(qq.typ);
                    adapter.notifyDataSetChanged();
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            messageArrayList.set(post, qq.error);
                            adapter.notifyDataSetChanged();
                            post++;
                        }
                    }, 700);
                }
            }
        }

        if (kk == 6) {
            if (!s.isEmpty()) {
                chk_qnFinish = false;
                ans.add(s);
                messageArrayList.add(qq.typ);
                adapter.notifyDataSetChanged();
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        messageArrayList.set(post, qq.dontwry);
                        adapter.notifyDataSetChanged();
                        post++;
                        messageArrayList.add(qq.typ);
                        adapter.notifyDataSetChanged();
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {

                                k++;
                                messageArrayList.set(post, qq.pleasure);
                                adapter.notifyDataSetChanged();
                                post++;

                                messageArrayList.add(qq.typ);
                                adapter.notifyDataSetChanged();

                                new Handler().postDelayed(new Runnable() {
                                    @Override
                                    public void run() {

                                        k++;
                                        messageArrayList.set(post, qq.endchat);
                                        adapter.notifyDataSetChanged();
                                        post++;
                                        Intent ii = new Intent(IntroChatActivity.this, MainActivity.class);

                                        ii.putExtra("answers", ans);

                                        startActivity(ii);

                                    }
                                }, 1000);
                            }
                        }, 1000);
                    }
                }, 1000);

            } else {
                messageArrayList.add(qq.typ);
                adapter.notifyDataSetChanged();
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        messageArrayList.set(post, qq.error);
                        adapter.notifyDataSetChanged();
                        post++;
                    }
                }, 700);
            }
        }


    }
}