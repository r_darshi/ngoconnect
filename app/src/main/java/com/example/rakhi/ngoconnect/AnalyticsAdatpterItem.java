package com.example.rakhi.ngoconnect;

public class AnalyticsAdatpterItem {

    String district, state, problem;


    public AnalyticsAdatpterItem(String district, String state, String problem) {
        this.district=district;
        this.state=state;
        this.problem=problem;
       
    }

    public String getdistrict() {
        return district;
    }

    public void setdistrict(String district) {
        this.district = district;
    }

    public String getstate() {
        return state;
    }

    public void setstate(String state) {
        this.state = state;
    }

    public String getproblem() {
        return problem;
    }

    public void setproblem(String problem) {
        this.problem = problem;
    }

}