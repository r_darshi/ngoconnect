package com.example.rakhi.ngoconnect;

import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.TextView;

import org.w3c.dom.Text;

public class ProblemDetailScreen extends AppCompatActivity {
    TextView posted_by, category;
    TextView prob;
    Button view_team;
    String description1, problemTitle1, location1, posted_by1, category1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_problem_detail_screen);

        prob = (TextView) findViewById(R.id.problem_name);
        posted_by = (TextView) findViewById(R.id.posted_by);
        category = (TextView) findViewById(R.id.category);
        view_team = (Button) findViewById(R.id.view_team);


        view_team.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(ProblemDetailScreen.this, TeamListActivity.class);
                startActivity(i);
            }
        });


        String vaal = getIntent().getStringExtra("Values");
        String token[] = vaal.split(";;");
        problemTitle1 = token[0].trim();
        location1 = token[1].trim();
        posted_by1 = token[2].trim();
        category1 = token[3].trim();
        description1 = token[4].trim();

        prob.setText(problemTitle1);
        posted_by.setText(posted_by1);
        category.setText(category1);
    }

    public void open_description(View v) {
        View DialogView;
        LayoutInflater factory = LayoutInflater.from(this);   //creating dilaog
        DialogView = factory.inflate(R.layout.simple_list_item, null);


        final AlertDialog dialog = new AlertDialog.Builder(this).create();
        dialog.setView(DialogView);
        dialog.show();

        TextView txt = (TextView) DialogView.findViewById(R.id.txt);
        txt.setText(description1);


    }

    public void locate_on_map(View v) {
        View DialogView;
        LayoutInflater factory = LayoutInflater.from(this);   //creating dilaog
        DialogView = factory.inflate(R.layout.webview, null);


        final AlertDialog dialog = new AlertDialog.Builder(this).create();
        dialog.setView(DialogView);
        dialog.show();

        WebView webView = (WebView) DialogView.findViewById(R.id.webview);
        webView.loadUrl("https://www.google.com/maps/search/?api=1&query=" + location1);


    }

    public void view_statistics(View v) {

        Intent i = new Intent(ProblemDetailScreen.this, Analytics.class);
        startActivity(i);

    }


}
