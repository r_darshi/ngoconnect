package com.example.rakhi.ngoconnect;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Rakhi on 12-10-2017.
 */
public class TeamListAdapter extends ArrayAdapter {

    ArrayList<TeamDetails> teamDet=new ArrayList<>();
    Context context;

    public TeamListAdapter(Context context, int resource,ArrayList<TeamDetails> teamDet) {
        super(context, resource);
        this.context=context;
        this.teamDet=teamDet;

    }

    @Override
    public int getCount() {
        return this.teamDet.size();
    }

    @Override
    public Object getItem(int position) {
        return this.teamDet.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override

    public View getView(int position, View convertView, ViewGroup parent) {
        View row=convertView;

        if(row==null){
            LayoutInflater inflater=(LayoutInflater)this.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row=inflater.inflate(R.layout.teamlist_item,parent,false);

        }
        TextView name=(TextView)row.findViewById(R.id.name);
        TextView category=(TextView)row.findViewById(R.id.category);

        TeamDetails team=teamDet.get(position);
        name.setText(team.name);
        category.setText(team.category);
        return row;
    }
}
