package com.example.rakhi.ngoconnect;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.util.ArrayList;

import droidninja.filepicker.FilePickerBuilder;
import droidninja.filepicker.FilePickerConst;

/**
 * Created by Rakhi on 12-10-2017.
 */
public class GalleryFragment extends Fragment {

    private File file;
    private String[] FilePathStrings;
    private String[] FileNameStrings;
    private File[] listFile;
    private GridView grid;
    private ImageAdapter adapter;


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        GridView gridView = (GridView)view. findViewById(R.id.grid_view);
        file = new File(Environment.getExternalStorageDirectory()
                + File.separator + "ChemistApp");
        // Create a new folder if no folder named ChemistApp exist
        file.mkdirs();


        if (file.isDirectory())

        {
            listFile = file.listFiles();
            // Create a String array for FilePathStrings
            FilePathStrings = new String[listFile.length];
            // Create a String array for FileNameStrings
            FileNameStrings = new String[listFile.length];


            for (int i = 1; i < listFile.length; i++) {
                // Get the path of the image file
                FilePathStrings[i] = listFile[i].getAbsolutePath();
                // Get the name image file
                FileNameStrings[i] = listFile[i].getName();
            }
        }

        // Pass String arrays to LazyAdapter Class
        adapter = new ImageAdapter(getActivity(), FilePathStrings, FileNameStrings);
        // Set the LazyAdapter to the GridView
        gridView.setAdapter(adapter);




        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            int c=0;

            public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {

                CheckBox checkBox=(CheckBox)view.findViewById(R.id.checkbox);
                checkBox.setVisibility(View.VISIBLE);
                adapter.setChecked(position);
                adapter.notifyDataSetChanged();

               /* Toast.makeText(GridAcivity.this, c=c+1, Toast.LENGTH_SHORT).show();
                attachments.setText("Attachments"+c);*/
            }
        });
        /*done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ///////////////////add picture to chemist app folder
                Intent i=new Intent(getContext(),MainActivity.class);
                imagepath_two=adapter.getArrayList();
                startActivity(i);
            }
        });*/

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.activity_grid,container,false);

        return view;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }
}
