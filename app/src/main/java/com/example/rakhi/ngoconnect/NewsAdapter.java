package com.example.rakhi.ngoconnect;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.media.Image;
import android.os.AsyncTask;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;

public class NewsAdapter extends ArrayAdapter {
    ArrayList<NewsItem> msg_list = new ArrayList<>();
    Context con;

    public NewsAdapter(Context context, int resource, ArrayList<NewsItem> msg_list) {
        super(context, resource);
        this.con = context;
        this.msg_list = msg_list;
    }

    @Override
    public int getCount() {
        return msg_list.size();
    }

    @Override
    public NewsItem getItem(int position) {
        return msg_list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        if (row == null) {
            LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row = inflater.inflate(R.layout.news_item, parent, false);

        }
        TextView title = (TextView) row.findViewById(R.id.news_topic);
        TextView author = (TextView) row.findViewById(R.id.news_author);
        ImageView img = (ImageView) row.findViewById(R.id.news_img);
        LinearLayout par = (LinearLayout) row.findViewById(R.id.news_parent);

        final NewsItem item = msg_list.get(position);

        title.setText(item.title);
        author.setText("Author: " + item.author);
        new DownloadImageTask(img)
                .execute(item.urlToImage);

        par.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                View DialogView;
                LayoutInflater factory = LayoutInflater.from(getContext());   //creating dilaog
                DialogView = factory.inflate(R.layout.webview, null);


                final AlertDialog dialog = new AlertDialog.Builder(getContext()).create();
                dialog.setView(DialogView);
                dialog.show();

                WebView webView = (WebView) DialogView.findViewById(R.id.webview);
                webView.loadUrl(item.url);
            }
        });

        return row;
    }


    private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
        ImageView bmImage;

        public DownloadImageTask(ImageView bmImage) {
            this.bmImage = bmImage;
        }

        protected Bitmap doInBackground(String... urls) {
            String urldisplay = urls[0];
            Bitmap mIcon11 = null;
            try {
                InputStream in = new java.net.URL(urldisplay).openStream();
                mIcon11 = BitmapFactory.decodeStream(in);
            } catch (Exception e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return mIcon11;
        }

        protected void onPostExecute(Bitmap result) {
            Drawable d = new BitmapDrawable(result);
            bmImage.setBackground(d);
        }
    }
}