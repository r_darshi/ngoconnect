package com.example.rakhi.ngoconnect;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

/**
 * Created by Rakhi on 14-10-2017.
 */
public class Description_one_frag extends Fragment {

    Animation fade_in;
ImageView logo;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.description_one,container,false);
    logo=(ImageView)view.findViewById(R.id.logo);
        fade_in= AnimationUtils.loadAnimation(getContext(),R.anim.fab_open);
        logo.startAnimation(fade_in);

        return view;
    }

}
