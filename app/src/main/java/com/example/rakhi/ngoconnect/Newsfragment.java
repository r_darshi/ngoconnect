package com.example.rakhi.ngoconnect;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Rakhi on 12-10-2017.
 */
public class Newsfragment extends Fragment {

    POSTConnection get_news = null;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.news, container, false);
    }

    JSONObject json;
    ProgressDialog pp;

    Utility utility = null;
    ArrayList<NewsItem> news;
    View v;


    @Override
    public void onViewCreated(final View view, @Nullable Bundle savedInstanceState) {


        news = new ArrayList<>();

        v = view;

        pp = new ProgressDialog(getContext());
        pp.setMessage("Loading...");
        pp.show();


//        HashMap<String, String> passable = new HashMap<>();
//
//        passable.put("source", "the-hindu");
//        passable.put("apiKey", "8801c0c9a86b4216be6924234b518c48");
//
//        get_news.setData(passable);
//        get_news.Connect();


        getJSON();

    }

    private void getJSON() {

        final String stringUrl = "https://newsapi.org/v1/articles?source=the-hindu&apiKey=8801c0c9a86b4216be6924234b518c48";
        class HttpGetRequest extends AsyncTask<Void, Void, String> {
            public static final String REQUEST_METHOD = "GET";
            public static final int READ_TIMEOUT = 15000;
            public static final int CONNECTION_TIMEOUT = 15000;


            @Override
            protected String doInBackground(Void... params) {
                String result = "";
                String inputLine;

                //Create a URL object holding our url
                URL myUrl = null;
                try {
                    myUrl = new URL(stringUrl);
                    //Create a connection
                    HttpURLConnection connection = (HttpURLConnection)
                            myUrl.openConnection();
                    //Set methods and timeouts
                    connection.setRequestMethod(REQUEST_METHOD);
                    connection.setReadTimeout(READ_TIMEOUT);
                    connection.setConnectTimeout(CONNECTION_TIMEOUT);

                    //Connect to our url
                    connection.connect();
                    //Create a new InputStreamReader
                    InputStreamReader streamReader = new
                            InputStreamReader(connection.getInputStream());
                    //Create a new buffered reader and String Builder
                    BufferedReader reader = new BufferedReader(streamReader);
                    StringBuilder stringBuilder = new StringBuilder();
                    //Check if the line we are reading is not null
                    while ((inputLine = reader.readLine()) != null) {
                        stringBuilder.append(inputLine);
                    }
                    //Close our InputStream and Buffered reader
                    reader.close();
                    streamReader.close();
                    //Set our result equal to our stringBuilder
                    result = stringBuilder.toString();
                } catch (MalformedURLException e) {

                } catch (ProtocolException e) {

                } catch (IOException e) {

                }

                return result;
            }

            protected void onPostExecute(String result) {
                super.onPostExecute(result);

                pp.dismiss();

                if(!result.isEmpty()) {

                    try {
                        json = new JSONObject(result);

                        Log.i("doOnSuccess tag", json.toString());

                        JSONArray array = json.getJSONArray("articles");
                        for (int i = 0; i < array.length(); i++) {
                            JSONObject o = array.getJSONObject(i);
                            NewsItem n = new NewsItem();

                            n.author = o.getString("author");
                            n.title = o.getString("title");
                            n.description = o.getString("description");
                            n.url = o.getString("url");
                            n.urlToImage = o.getString("urlToImage");

                            news.add(n);

                        }

                        ListView newsList = (ListView) v.findViewById(R.id.newsList);

                        NewsAdapter adapter = new NewsAdapter(getContext(), R.layout.news_item, news);
                        newsList.setAdapter(adapter);


                    } catch (JSONException e) {

                        Toast.makeText(getActivity().getApplicationContext(), "Could not be fetched!!", Toast.LENGTH_SHORT).show();
                    }
                }
                else{
                    //utility.error("No data network!!");
                }
            }

            @Override
            protected void onPreExecute() {
                utility = new Utility(getActivity());
                if (!utility.isNetworkConnected()) {
                    utility.error("No internet Connection");
                }


            }
        }
        HttpGetRequest gj = new HttpGetRequest();
        gj.execute();
    }
}
