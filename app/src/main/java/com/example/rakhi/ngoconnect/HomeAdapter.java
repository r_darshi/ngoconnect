package com.example.rakhi.ngoconnect;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.bumptech.glide.load.engine.Resource;

import java.util.ArrayList;

/**
 * Created by Rakhi on 12-10-2017.
 */
public class HomeAdapter extends ArrayAdapter<Issues>{

    ArrayList<Issues> issues=new ArrayList<>();
    Context context;
    Animation fade_in;


    public HomeAdapter(Context context, int resource,ArrayList<Issues> issues) {
        super(context, resource);
        this.context=context;
        this.issues=issues;
    }


    @Override
    public int getCount() {
        return this.issues.size();
    }

    @Override
    public Issues getItem(int position) {
        return this.issues.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row=convertView;
        if(row==null){
            LayoutInflater inflater=(LayoutInflater)this.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row=inflater.inflate(R.layout.home_list_item,parent,false);
        }

        TextView prob=(TextView)row.findViewById(R.id.problem_title);
        TextView loc=(TextView)row.findViewById(R.id.location);
        Issues is=issues.get(position);
        prob.setText(is.problemTitle);
        loc.setText(is.location);
        fade_in= AnimationUtils.loadAnimation(context,R.anim.fab_open);
        row.startAnimation(fade_in);

        return row;
    }
}
