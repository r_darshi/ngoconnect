package com.example.rakhi.ngoconnect;

import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ListView;
import java.util.ArrayList;

public class ChatActivity extends AppCompatActivity {
    ListView chatlist;
    EditText message;
    FloatingActionButton flt_btn;
    ArrayList<String> msg_list=new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);
        chatlist=(ListView)findViewById(R.id.chatlist);

        flt_btn=(FloatingActionButton)findViewById(R.id.float_btn);
        final ChatAdapter adapter=new ChatAdapter(ChatActivity.this,R.layout.message_item,msg_list);
        chatlist.setAdapter(adapter);

        flt_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                message=(EditText)findViewById(R.id.message);
                String msg=message.getText().toString().trim();
                Log.i("Message",msg);
                msg_list.add(msg);
                Log.i("MessageList",String.valueOf(msg_list));

                adapter.notifyDataSetChanged();

            }
        });


    }
}
