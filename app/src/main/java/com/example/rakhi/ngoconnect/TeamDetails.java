package com.example.rakhi.ngoconnect;

/**
 * Created by Rakhi on 12-10-2017.
 */
public class TeamDetails {
    String name,category,email,phone;


    public TeamDetails(String name, String category, String email, String phone) {
        this.name=name;
        this.category=category;
        this.email=email;
        this.phone=phone;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getcategory() {
        return category;
    }

    public void setcategory(String category) {
        this.category = category;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
}
