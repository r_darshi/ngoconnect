package com.example.rakhi.ngoconnect;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.zip.Inflater;

/**
 * Created by Rakhi on 12-10-2017.
 */
public class ChatAdapter extends ArrayAdapter<String> {

    ArrayList<String> msg_list=new ArrayList<>();
    Context con;

    public ChatAdapter(Context context, int resource, ArrayList<String> msg_list) {
        super(context, resource);
        this.con=context;
        this.msg_list=msg_list;
    }

    @Override
    public int getCount() {
        return msg_list.size();
    }

    @Override
    public String getItem(int position) {
        return msg_list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row=convertView;
        if(row==null){
            LayoutInflater inflater=(LayoutInflater)getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row=inflater.inflate(R.layout.message_item,parent,false);

        }
        TextView txt_msg=(TextView)row.findViewById(R.id.txt_msg);

        String msg=msg_list.get(position);
        Log.i("Message in adapter",msg);
        Log.i("Message in adapter",msg);
        Log.i("Message in adapter",msg);
        Log.i("Message in adapter",msg);
        Log.i("Message in adapter",msg);

        txt_msg.setText(msg);


        return row;
    }
}
