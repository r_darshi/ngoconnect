package com.example.rakhi.ngoconnect;

import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

public class TeamListActivity extends AppCompatActivity {

    ListView team_list;
    ArrayList<TeamDetails> teamdet = new ArrayList<>();
    TeamListAdapter ad;
    Button join_team, enter_team;
    AlertDialog dialog;
    String cat = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_team_list);

        team_list = (ListView) findViewById(R.id.team_list);
        join_team = (Button) findViewById(R.id.join);
        enter_team = (Button) findViewById(R.id.enter);

        teamdet.add(new TeamDetails("Rakhi", "NGO", "darshirakhi@gmail.com", "1234567890"));
        teamdet.add(new TeamDetails("Rakhi", "Volunteer", "darshirakhi@gmail.com", "1234567890"));
        teamdet.add(new TeamDetails("Rakhi", "Others", "darshirakhi@gmail.com", "1234567890"));

        ad = new TeamListAdapter(TeamListActivity.this, R.layout.teamlist_item, teamdet);
        team_list.setAdapter(ad);
        join_team.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                View DialogView;
                LayoutInflater factory = LayoutInflater.from(TeamListActivity.this);   //creating dilaog
                DialogView = factory.inflate(R.layout.are_you, null);


                dialog = new AlertDialog.Builder(TeamListActivity.this).create();
                dialog.setView(DialogView);
                dialog.show();


                enter_team.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent i = new Intent(TeamListActivity.this, ChatActivity.class);
                        startActivity(i);
                    }
                });
            }
        });


    }


    public void ngo(View v) {
        Toast.makeText(TeamListActivity.this, "You have successfully joined the team", Toast.LENGTH_SHORT).show();
        join_team.setVisibility(View.GONE);
        enter_team.setVisibility(View.VISIBLE);
        cat = "NGO";
        teamdet.add(new TeamDetails("Nishijeet", cat, "nishijeet@gmail.com", "7542012513"));
        ad.notifyDataSetChanged();
        dialog.dismiss();
    }

    public void others(View v) {
        Toast.makeText(TeamListActivity.this, "You have successfully joined the team", Toast.LENGTH_SHORT).show();
        join_team.setVisibility(View.GONE);
        enter_team.setVisibility(View.VISIBLE);
        cat = "Others";
        teamdet.add(new TeamDetails("Nishijeet", cat, "nishijeet@gmail.com", "7542012513"));
        ad.notifyDataSetChanged();
        dialog.dismiss();

    }

    public void investor(View v) {
        Toast.makeText(TeamListActivity.this, "You have successfully joined the team", Toast.LENGTH_SHORT).show();
        join_team.setVisibility(View.GONE);
        enter_team.setVisibility(View.VISIBLE);
        cat = "Investor/Donator";
        teamdet.add(new TeamDetails("Nishijeet", cat, "nishijeet@gmail.com", "7542012513"));
        ad.notifyDataSetChanged();
        dialog.dismiss();

    }

    public void volunteer(View v) {
        Toast.makeText(TeamListActivity.this, "You have successfully joined the team", Toast.LENGTH_SHORT).show();
        join_team.setVisibility(View.GONE);
        enter_team.setVisibility(View.VISIBLE);
        cat = "Volunteer";
        teamdet.add(new TeamDetails("Nishijeet", cat, "nishijeet@gmail.com", "7542012513"));
        ad.notifyDataSetChanged();
        dialog.dismiss();

    }

    public void sw(View v) {
        Toast.makeText(TeamListActivity.this, "You have successfully joined the team", Toast.LENGTH_SHORT).show();
        join_team.setVisibility(View.GONE);
        enter_team.setVisibility(View.VISIBLE);
        cat = "Social Worker";
        teamdet.add(new TeamDetails("Nishijeet", cat, "nishijeet@gmail.com", "7542012513"));
        ad.notifyDataSetChanged();
        dialog.dismiss();

    }
}
