package com.example.rakhi.ngoconnect;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.RelativeLayout;

import java.util.ArrayList;

/**
 * Created by Rakhi on 12-10-2017.
 */
public class HomeFragment extends Fragment {
    private ListView listView;
    private ArrayList<Issues> issues = new ArrayList<>();
    HomeAdapter adapter;
    ArrayList<String> newPost = new ArrayList<>();


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        listView = (ListView) view.findViewById(R.id.list_startup);
        listView.setNestedScrollingEnabled(true);

        newPost = (ArrayList<String>) getActivity().getIntent().getSerializableExtra("answers");

        if (newPost != null) {
            issues.add(new Issues(newPost.get(2), newPost.get(4) + ", " + newPost.get(5), newPost.get(0) + ", " + newPost.get(1), "Others", newPost.get(3)));
        }

        issues.add(new Issues("Increasing Rape cases in UP", "Uttar Pradesh", "Rakhi darshi", "Crime Against Women", "Rising crime is eating up the society! A person robbed in mid road."));
        issues.add(new Issues("Increasing Rape cases in UP", "Uttar Pradesh", "Rakhi darshi", "Crime Against Women", "Rising crime is eating up the society! A person robbed in mid road."));
        issues.add(new Issues("Increasing Rape cases in UP", "Uttar Pradesh", "Rakhi darshi", "Crime Against Women", "Rising crime is eating up the society! A person robbed in mid road."));
        issues.add(new Issues("Increasing Rape cases in UP", "Uttar Pradesh", "Rakhi darshi", "Crime Against Women", "Rising crime is eating up the society! A person robbed in mid road."));
        issues.add(new Issues("Increasing Rape cases in UP", "Uttar Pradesh", "Rakhi darshi", "Crime Against Women", "Rising crime is eating up the society! A person robbed in mid road."));

        adapter = new HomeAdapter(getContext(), R.layout.home_list_item, issues);///////getContext
        listView.setAdapter(adapter);


        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Issues is = issues.get(position);
                String val = is.getProblemTitle() + ";;" + is.getLocation() + ";;" + is.getPosted_by() + ";;" + is.getCategory() + ";;" + is.getDescription();
                Intent i = new Intent(getActivity(), ProblemDetailScreen.class);
                i.putExtra("Values", val);
                startActivity(i);
            }
        });


        FloatingActionButton chat = (FloatingActionButton) view.findViewById(R.id.chat);
        chat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), IntroChatActivity.class));
            }
        });


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        return inflater.inflate(R.layout.activity_home, container, false);
    }


}
