package com.example.rakhi.ngoconnect;

import android.graphics.DashPathEffect;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.series.BarGraphSeries;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;
import com.jjoe64.graphview.series.PointsGraphSeries;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;

public class ShowGraph extends AppCompatActivity {

    LinearLayout lyy;
    private Animation fab_open, fab_close;
    private Boolean isFabOpen = false;
    GraphView graph;
    String toBe = "";
    String text = "";
    ListView yearList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.graph);

        ValuesFolder vv = new ValuesFolder();

        final String selected_district = getIntent().getExtras().getString("selected");
        final String item = getIntent().getExtras().getString("item");
        if (item.equals("0")) {
            toBe = vv.crime_children_01x12;
        } else if (item.equals("1")) {
            toBe = vv.crime_women_1x12;
        }

        text = vv.indexing(item);

        TextView txt = (TextView) findViewById(R.id.txt);
        txt.setText(text);

        Log.i("thzzthy", selected_district);

        lyy = (LinearLayout) findViewById(R.id.layy);

        fab_open = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.open);
        fab_close = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.close);

        graph = (GraphView) findViewById(R.id.graph);

        FloatingActionButton drop = (FloatingActionButton) findViewById(R.id.drop);
        drop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                animateFAB();
            }
        });


        yearList = (ListView) findViewById(R.id.yearList);

        final ArrayList<String> years = new ArrayList<>();
        years.add("2008");
        years.add("2009");
        years.add("2010");
        yearList.setAdapter(new ArrayAdapter<String>(this, R.layout.simple_list_item, years));

        final ArrayList<String> data = new Parsors().jsonToArray(toBe);
        final int l = new Parsors().DataNumber(toBe);

        final String[][] ar = new String[data.size()][l];

        for (int i = 0; i < data.size(); i++) {

            try {
                Log.i("MainWalathzzz", "entered");
                JSONArray array = new JSONArray(data.get(i));
                for (int j = 0; j < l; j++) {

                    ar[i][j] = array.getString(j);
                    Log.i("MainWalathzzz", ar[i][j]);
                }
            } catch (JSONException e) {
                Log.i("MainWalathzzz", "caught");
            }


        }

        final int le = l - 3;
        final String req[] = new String[le];

        yearList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                graph.removeAllSeries();
                String selected_year = years.get(position);
                Log.i("Selectedthzz", "|" + selected_year + "|" + selected_district + "|");

                for (int i = 0; i < data.size(); i++) {

                    if (ar[i][1].equals(selected_district) && ar[i][2].equals(selected_year)) {

                        Log.i("FillingRequiredthzz", "Inside");

                        for (int k = 0; k < le; k++) {

                            req[k] = ar[i][k + 3];
                            Log.i("FillingRequiredthzz", req[k]);
                        }

                    }

                }

                DataPoint[] d = new DataPoint[le];
                for (int k = 0; k < le; k++) {
                    d[k] = new DataPoint(k, Double.parseDouble(req[k]));
                }

                BarGraphSeries<DataPoint> series = new BarGraphSeries<>(d);


                graph.addSeries(series);

                LineGraphSeries<DataPoint> series1 = new LineGraphSeries<DataPoint>(d);

                series1.setColor(R.color.colorAccent);
                graph.addSeries(series1);

            }
        });


    }

    public void animateFAB() {  // animate the expandable floatingAction Button

        if (isFabOpen) {
            lyy.startAnimation(fab_close);
            lyy.setClickable(false);
            isFabOpen = false;
            yearList.setEnabled(false);
            Log.d("nj", "close");

        } else {
            lyy.startAnimation(fab_open);
            lyy.setClickable(true);
            isFabOpen = true;
            yearList.setEnabled(true);
            Log.d("nj", "open");

        }
    }
}