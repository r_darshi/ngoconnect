package com.example.rakhi.ngoconnect;

/**
 * Created by USER on 13-06-2017.
 */
public class Questions {

    public ChatMessage qintro = new ChatMessage(false, "Hello " + "Sir");
    public ChatMessage typ = new ChatMessage(false, "Typing...");

    public ChatMessage q0 = new ChatMessage(false, "What is your name?");//0

    public ChatMessage q1 = new ChatMessage(false, "What are you?\na. NGO\nb. Social Worker\nc. Donator\nd. Others");//1

    public ChatMessage q3 = new ChatMessage(false, "What problem do you want to highlight?");//2

    public ChatMessage endchat = new ChatMessage(false, "No more questions, Thank you!!");

    public ChatMessage error = new ChatMessage(false, "Please submit a valid option!");

    public ChatMessage q4= new ChatMessage(false, "Any Description about the problem?");//3

    public ChatMessage q5 = new ChatMessage(false, "What is the district?");//4

    public ChatMessage okay = new ChatMessage(false, "Okay! Got it.");

    public ChatMessage q6 = new ChatMessage(false, "What is the state?");//5

    public ChatMessage q8 = new ChatMessage(false, "Your answers have been recorded.");

    public ChatMessage pleasure = new ChatMessage(false, "It was a pleasure talking to you.");

    public ChatMessage dontwry = new ChatMessage(false, "The details will be posted.");


    public ChatMessage okay3 = new ChatMessage(false, "Noted!.");
    public ChatMessage okay6 = new ChatMessage(false, "Alright.");

}
