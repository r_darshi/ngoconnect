package com.example.rakhi.ngoconnect;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.series.BarGraphSeries;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;

import java.util.ArrayList;
import java.util.Random;

public class ShowGraph2 extends AppCompatActivity {

    LinearLayout lyy;
    private Animation fab_open, fab_close;
    private Boolean isFabOpen = false;
    GraphView graph;
    String text = "";

    ListView yearList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.graph);

        final Random r = new Random();

        int length = 0;

        ValuesFolder vv = new ValuesFolder();

        final String selected_district = getIntent().getExtras().getString("selected");
        final String item = getIntent().getExtras().getString("item");

        if (item.equals("2")) {
            length = vv.index_healthCenters.length;
            text = vv.indexing("2");
        } else if (item.equals("3")) {
            length = vv.index_dugwells_irrigation.length;
            text = vv.indexing("3");

        } else if (item.equals("4")) {
            length = vv.index_farmers_suicide.length;
            text = vv.indexing("4");

        }


        TextView txt = (TextView) findViewById(R.id.txt);
        txt.setText(text);

        Log.i("thzzthy", selected_district);

        lyy = (LinearLayout) findViewById(R.id.layy);

        fab_open = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.open);
        fab_close = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.close);

        graph = (GraphView) findViewById(R.id.graph);

        FloatingActionButton drop = (FloatingActionButton) findViewById(R.id.drop);
        drop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                animateFAB();
            }
        });


        yearList = (ListView) findViewById(R.id.yearList);

        final ArrayList<String> years = new ArrayList<>();
        years.add("2008");
        years.add("2009");
        years.add("2010");
        yearList.setAdapter(new ArrayAdapter<String>(this, R.layout.simple_list_item, years));


        final int finalLength = length;
        yearList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                graph.removeAllSeries();
                String selected_year = years.get(position);
                Log.i("Selectedthzz", "|" + selected_year + "|" + selected_district + "|");

                DataPoint[] d = new DataPoint[finalLength];
                for (int k = 0; k < finalLength; k++) {
                    d[k] = new DataPoint(k, r.nextInt(30));
                }

                BarGraphSeries<DataPoint> series = new BarGraphSeries<>(d);


                graph.addSeries(series);

                LineGraphSeries<DataPoint> series1 = new LineGraphSeries<DataPoint>(d);

                series1.setColor(R.color.colorAccent);
                graph.addSeries(series1);


            }
        });


    }


    public void animateFAB() {  // animate the expandable floatingAction Button

        if (isFabOpen) {
            lyy.startAnimation(fab_close);
            lyy.setClickable(false);
            isFabOpen = false;
            yearList.setEnabled(false);
            Log.d("nj", "close");

        } else {
            lyy.startAnimation(fab_open);
            lyy.setClickable(true);
            yearList.setEnabled(true);
            isFabOpen = true;
            Log.d("nj", "open");

        }
    }

}