package com.example.rakhi.ngoconnect;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.series.DataPoint;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.TreeSet;

public class DistrictWiseFrag1 extends Fragment {

    ArrayList<AnalyticsAdatpterItem> items;
    ArrayList<String> data;
    TreeSet<String> dist;

    LinearLayout lyy;
    private Animation fab_open, fab_close;
    private Boolean isFabOpen = false;
    LinearLayout layy;

    ListView district_wise, districts;


    public DistrictWiseFrag1() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.district_wise_frag1, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {

        items = new ArrayList<>();
        data = new ArrayList<>();
        dist = new TreeSet<>();

        fab_open = AnimationUtils.loadAnimation(getContext().getApplicationContext(), R.anim.open);
        fab_close = AnimationUtils.loadAnimation(getContext().getApplicationContext(), R.anim.close);

        final ValuesFolder vv = new ValuesFolder();

        data = new Parsors().jsonToArray(vv.crime_children_01x12);

        final String[][] ar = new String[data.size()][vv.index_crime_children_01x12.length];

        for (int i = 0; i < data.size(); i++) {

            try {
                Log.i("MainWalathzzz", "entered");
                JSONArray array = new JSONArray(data.get(i));
                for (int j = 0; j < vv.index_crime_children_01x12.length; j++) {

                    ar[i][j] = array.getString(j);
                    Log.i("MainWalathzzz", ar[i][j]);
                }
            } catch (JSONException e) {
                Log.i("MainWalathzzz", "caught");
            }


        }

        final TextView district_name = (TextView) view.findViewById(R.id.district_name);
        FloatingActionButton drop = (FloatingActionButton) view.findViewById(R.id.drop);
        lyy = (LinearLayout) view.findViewById(R.id.layy);

        district_name.setText("Select district!");
        final boolean[] chk = {true};
        drop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                animateFAB();
            }

        });

        districts = (ListView) view.findViewById(R.id.districts);

        for (int i = 0; i < data.size(); i++) {

            dist.add(ar[i][1] + " ," + ar[i][0]);
        }

        Iterator iterator = dist.iterator();
        final ArrayList<String> dist1 = new ArrayList<>();


        while (iterator.hasNext()) {
            dist1.add(iterator.next().toString());
        }

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(), R.layout.simple_list_item, dist1);
        districts.setAdapter(adapter);

        district_wise = (ListView) view.findViewById(R.id.districtwise_list);
        final String[] selected_district = {""};

        districts.setOnItemClickListener(new AdapterView.OnItemClickListener()

                                         {
                                             @Override
                                             public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                                 ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(), R.layout.simple_list_item, vv.problems_districtWise);
                                                 district_wise.setAdapter(adapter);
                                                 String s = dist1.get(position);
                                                 String tok[] = s.split("\\,");
                                                 tok[0] = tok[0].trim();
                                                 selected_district[0] = tok[0];
                                                 district_name.setText(s);
                                             }
                                         }

        );


        district_wise.setOnItemClickListener(new AdapterView.OnItemClickListener()

                                             {
                                                 @Override
                                                 public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                                                     if (position == 0) {


                                                         Intent i = new Intent(getActivity(), ShowGraph.class);
                                                         i.putExtra("selected", selected_district[0]);
                                                         i.putExtra("item", "0");
                                                         startActivity(i);

                                                     } else if (position == 1) {


                                                         Intent i = new Intent(getActivity(), ShowGraph.class);
                                                         i.putExtra("selected", selected_district[0]);
                                                         i.putExtra("item", "1");
                                                         startActivity(i);

                                                     } else if (position == 2) {


                                                         Intent i = new Intent(getActivity(), ShowGraph2.class);
                                                         i.putExtra("selected", selected_district[0]);
                                                         i.putExtra("item", "2");
                                                         startActivity(i);

                                                     } else if (position == 3) {


                                                         Intent i = new Intent(getActivity(), ShowGraph2.class);
                                                         i.putExtra("selected", selected_district[0]);
                                                         i.putExtra("item", "3");
                                                         startActivity(i);

                                                     } else if (position == 4) {


                                                         Intent i = new Intent(getActivity(), ShowGraph2.class);
                                                         i.putExtra("selected", selected_district[0]);
                                                         i.putExtra("item", "4");
                                                         startActivity(i);

                                                     }


                                                 }
                                             }

        );


    }

    public void animateFAB() {  // animate the expandable floatingAction Button

        if (isFabOpen) {
            lyy.startAnimation(fab_close);
            lyy.setClickable(false);
            isFabOpen = false;
            districts.setEnabled(false);
            districts.setClickable(false);
            districts.setFocusable(false);
            Log.d("nj", "close");

        } else {
            lyy.startAnimation(fab_open);
            lyy.setClickable(true);
            isFabOpen = true;
            districts.setClickable(true);
            districts.setEnabled(true);
            districts.setFocusable(true);
            Log.d("nj", "open");

        }
    }
}