package com.example.rakhi.ngoconnect;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.firebase.auth.FirebaseAuth;

import java.util.ArrayList;
import java.util.List;


public class MainActivity extends AppCompatActivity {

    private Toolbar toolbar;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private ImageView image;
    private Animation fab_open, fab_close, rotate_forward, rotate_backward, right_left, left_right;
    private Boolean isFabOpen = false;
    private FloatingActionButton fab;
    private LinearLayout profile_fab, analytics_fab, logout_fab;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_main);

        image=(ImageView)findViewById(R.id.image);
        CustomCyclicTransitionDrawable ctd=new CustomCyclicTransitionDrawable(new Drawable[]{
                ContextCompat.getDrawable(MainActivity.this,R.drawable.women),
                ContextCompat.getDrawable(MainActivity.this,R.drawable.child),
                ContextCompat.getDrawable(MainActivity.this,R.drawable.framer),
                ContextCompat.getDrawable(MainActivity.this,R.drawable.health)


        });
        image.setImageDrawable(ctd);
        ctd.startTransition(1000, 2000) ;// 1 second transition, 3 second pause between transitions.

        final CollapsingToolbarLayout collapsingToolbarLayout =
                (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar);
        collapsingToolbarLayout.setTitle(" ");
        AppBarLayout appBarLayout = (AppBarLayout) findViewById(R.id.appbar);


        appBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            boolean isShow = false;
            int scrollRange = -1;

            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                if (scrollRange == -1) {
                    scrollRange = appBarLayout.getTotalScrollRange();
                }
                if (scrollRange + verticalOffset == 0) {
                    collapsingToolbarLayout.setTitle("Ngo");
                    isShow = true;
                } else if (isShow) {
                    collapsingToolbarLayout.setTitle(" ");
                    isShow = false;
                }
            }
        });


        viewPager = (ViewPager) findViewById(R.id.viewpager);
        setupViewPager(viewPager);


        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);

        //////////////////animations/////////////////////

        fab_open = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fab_open);
        fab_close = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fab_close);
        left_right = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.left_right);
        right_left = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.right_left);
        rotate_forward = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.rotate_forward);
        rotate_backward = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.rotate_backward);

        fab = (FloatingActionButton) findViewById(R.id.parent);
        profile_fab = (LinearLayout) findViewById(R.id.above_w);
        analytics_fab = (LinearLayout) findViewById(R.id.left_w);
        logout_fab = (LinearLayout) findViewById(R.id.right_w);
       // expand1 = (RelativeLayout) findViewById(R.id.expand1);


        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                animateFAB();
            }
        });
///////////////////////////////// aniation over//////////////////////////////

        //////////////////onclicks///////////////////
        profile_fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, Profile.class));
            }
        });


        analytics_fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, Analytics.class));
            }
        });

        logout_fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(MainActivity.this, "Logged Out!!", Toast.LENGTH_SHORT).show();
            }
        });


    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new HomeFragment(), "  Home ");
        adapter.addFragment(new Newsfragment(), "  News ");
        adapter.addFragment(new GalleryFragment(), "Gallery");

        viewPager.setAdapter(adapter);
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }


    public void animateFAB() {

        if (isFabOpen) {

            fab.startAnimation(rotate_backward);
            profile_fab.startAnimation(fab_close);
            analytics_fab.startAnimation(fab_close);
            logout_fab.startAnimation(fab_close);
         //   expand1.startAnimation(fab_close);
            profile_fab.setClickable(false);
            analytics_fab.setClickable(false);
            logout_fab.setClickable(false);
           // expand1.setClickable(false);
            isFabOpen = false;
            Log.d("nj", "close");

        } else {

            fab.startAnimation(rotate_forward);
            profile_fab.startAnimation(fab_open);
            analytics_fab.startAnimation(fab_open);
            logout_fab.startAnimation(fab_open);
            //expand1.setAnimation(fab_open);
            profile_fab.setClickable(true);
            analytics_fab.setClickable(true);
            logout_fab.setClickable(true);
           // expand1.setClickable(true);
            isFabOpen = true;
            Log.d("nj", "open");

        }
    }
    @Override
    protected void onResume() {
        // TODO Auto-generated method stub
        super.onResume();
    }


}