package com.example.rakhi.ngoconnect;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;
import java.util.TreeSet;

public class StandardFrag2 extends Fragment {

    ArrayList<AnalyticsAdatpterItem> items;
    ArrayList<String> data;
    TreeSet<String> dist;

    LinearLayout lyy;
    private Animation fab_open, fab_close;
    private Boolean isFabOpen = false;
    LinearLayout layy;


    public StandardFrag2() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.standard_frag2, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {

        ListView list = (ListView) view.findViewById(R.id.list);
        ValuesFolder vv = new ValuesFolder();

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(), R.layout.simple_list_item, vv.problems_standard);
        list.setAdapter(adapter);

        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
               startActivity(new Intent(getActivity(),ShowGraphStandard.class));
            }
        });


    }
}