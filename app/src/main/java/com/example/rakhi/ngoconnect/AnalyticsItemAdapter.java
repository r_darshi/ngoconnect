package com.example.rakhi.ngoconnect;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;

public class AnalyticsItemAdapter extends ArrayAdapter {
    ArrayList<AnalyticsAdatpterItem> msg_list = new ArrayList<>();
    Context con;

    public AnalyticsItemAdapter(Context context, int resource, ArrayList<AnalyticsAdatpterItem> msg_list) {
        super(context, resource);
        this.con = context;
        this.msg_list = msg_list;
    }

    @Override
    public int getCount() {
        return msg_list.size();
    }

    @Override
    public AnalyticsAdatpterItem getItem(int position) {
        return msg_list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        if (row == null) {
            LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row = inflater.inflate(R.layout.message_item, parent, false);

        }
        TextView problem = (TextView) row.findViewById(R.id.problem);
        TextView place = (TextView) row.findViewById(R.id.place);

        AnalyticsAdatpterItem item = msg_list.get(position);

        problem.setText(item.getproblem());
        place.setText(item.getdistrict() + " , " + item.getstate());


        return row;
    }
}