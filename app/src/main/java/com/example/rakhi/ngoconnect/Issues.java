package com.example.rakhi.ngoconnect;

/**
 * Created by Rakhi on 12-10-2017.
 */
public class Issues {
    String problemTitle, location, posted_by, category, description;

    public Issues(String problemTitle, String location, String posted_by, String category, String description) {
        this.problemTitle = problemTitle;
        this.location = location;
        this.posted_by = posted_by;
        this.category = category;
        this.description = description;

    }

    public String getPosted_by() {
        return posted_by;
    }

    public void setPosted_by(String posted_by) {
        this.posted_by = posted_by;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getProblemTitle() {
        return problemTitle;
    }

    public void setProblemTitle(String problemTitle) {
        this.problemTitle = problemTitle;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
